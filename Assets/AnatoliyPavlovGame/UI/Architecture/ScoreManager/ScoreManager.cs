﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text currentScoreValue;
    public Text totalScoreValue;
    private float scoreValue;

    private void Awake()
    {
        CurrentScore.Initialize();
    }
    private void Start()
    {
        scoreValue = 0;
    }
    private void Update()
    {
        if (Score.isInitialized)
        {
            currentScoreValue.text = $"{CurrentScore.score}";
            totalScoreValue.text = $"{Score.score}";
        }
    }
}
