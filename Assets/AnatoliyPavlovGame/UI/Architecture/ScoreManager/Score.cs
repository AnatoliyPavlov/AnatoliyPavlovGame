﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Score
{
    //public static event Action OnScoreInitializeEvent;
    public static int score
    {
        get
        {
            CheckClass();
            return scoreInteractor.score;
        }
    }
    public static bool isInitialized { get; private set; }

    private static ScoreInteractor scoreInteractor;

    public static void Initialize(ScoreInteractor interactor)
    {
        scoreInteractor = interactor;
        isInitialized = true;
        //OnScoreInitializeEvent?.Invoke();
    }

    public static void AddScore(object sender, int value)
    {
        CheckClass();
        scoreInteractor.AddScore(sender, value);
    }

    private static void CheckClass()
    {
        if (!isInitialized)
            throw new Exception("Score is not initialize yet");
    }
}
