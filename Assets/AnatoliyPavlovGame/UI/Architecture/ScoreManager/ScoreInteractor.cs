﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreInteractor : Interactor
{
    private ScoreRepository repository;

    public int score => this.repository.score;

    public override void OnCreate()
    {
        base.OnCreate();
        this.repository = SceneConfigLoader.scene.GetRepository<ScoreRepository>();
    }
    public override void Initialize()
    {
        Score.Initialize(this);
    }
    public void AddScore(object sender, int value)
    {
        this.repository.score += value;
        this.repository.Save();
    }
}
