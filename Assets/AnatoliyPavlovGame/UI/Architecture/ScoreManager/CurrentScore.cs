﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CurrentScore
{
    public static int score { get; private set; }

    public static bool isInitialized { get; private set; }

    public static void Initialize()
    {
        score = 0;
        isInitialized = true;
    }
    public static void AddScore(int value)
    {
        score+=value;
    }
}
