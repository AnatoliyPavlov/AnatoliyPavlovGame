﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private float musicVolumeValue;
    [SerializeField]
    private float miscVolumeValue;
    [SerializeField]
    private Slider SliderVolumeMusic;
    [SerializeField]
    private Slider SliderVolumeMisc;
    [SerializeField]
    private Text LabelVolumeMusicValue;
    [SerializeField]
    private Text LabelVolumeMiscValue;
    [SerializeField]
    private float timeScale;
    [SerializeField]
    private GameObject menu;
    [SerializeField]
    private AudioMixer audioMixer;

    private void Start()
    {
        Initialized();
    }

    private void Initialized()
    {
        if (PlayerPrefs.HasKey("SavedmusicVolumeValue"))
        {
            LoadSettings();
        }
        else
        {
            ResetSettings();
            SaveSettings();
        }
        if (SliderVolumeMusic)
        {
            SliderVolumeMusic.value = musicVolumeValue;
            LabelVolumeMusicValue.text = musicVolumeValue.ToString();
        }
        if (SliderVolumeMisc)
        {
            SliderVolumeMisc.value = miscVolumeValue;
            LabelVolumeMiscValue.text = miscVolumeValue.ToString();
        }
        audioMixer = Resources.Load<AudioMixer>("SoundsMixer");
        //Debug.Log("Music volume value is " + musicVolumeValue);
        //Debug.Log("Misc volume value is " + miscVolumeValue);
    }

    public void UIStartGameButtonClick()
    {
        //Debug.Log("StartButton!");
        SceneLoader sceneLoader = new SceneLoader();
        sceneLoader.LoadNextScene();
    }

    public void UIExitButtonClick()
    {
        //Debug.Log("ExitButton!");
        Application.Quit();
    }

    private void SaveSettings()
    {
        PlayerPrefs.SetFloat("SavedmusicVolumeValue", musicVolumeValue);
        PlayerPrefs.SetFloat("SavedmiscVolumeValue", miscVolumeValue);
        PlayerPrefs.Save();
        //Debug.Log("Game data saved!");
    }

    private void LoadSettings()
    {
        musicVolumeValue = PlayerPrefs.GetFloat("SavedmusicVolumeValue");
        miscVolumeValue = PlayerPrefs.GetFloat("SavedmiscVolumeValue");
        //Debug.Log("Game data loaded!");
    }

    private void ResetSettings()
    {
        PlayerPrefs.DeleteAll();
        musicVolumeValue = 100f;
        miscVolumeValue = 100f;
        //Debug.Log("Data reset complete");
    }

    public void UISliderMusicChangeVolume()
    {
        musicVolumeValue = (int)SliderVolumeMusic.value;
        LabelVolumeMusicValue.text = musicVolumeValue.ToString();
        audioMixer.SetFloat("MusicVolume", Mathf.Log10(musicVolumeValue * 0.01f + 0.001f) * 20);
    }

    public void UISliderMiscChangeVolume()
    {
        miscVolumeValue = (int)SliderVolumeMisc.value;
        LabelVolumeMiscValue.text = miscVolumeValue.ToString();
        audioMixer.SetFloat("MiscVolume", Mathf.Log10(miscVolumeValue * 0.01f + 0.001f) * 20);
    }

    public void UISliderOnPointerUpSave()
    {
        SaveSettings();
    }

    public float GetMusicVolumeValue()
    {
        return this.musicVolumeValue;
    }

    public float GetMiscVolumeValue()
    {
        return this.miscVolumeValue;
    }

    public void UIMenuButtonClick()
    {
        if (Time.timeScale != 0)
        {
            this.timeScale = Time.timeScale;
            Time.timeScale = 0;
            this.menu.SetActive(true);
        }
        else
        {
            Time.timeScale = this.timeScale;
            this.menu.SetActive(false);
        }
    }

    public void UIExitToStartButtonClick()
    {
        //Debug.Log("ExitToStartButton!");
        Time.timeScale = this.timeScale;
        SceneLoader sceneLoader = new SceneLoader();
        sceneLoader.LoadStartScene();
    }
}
