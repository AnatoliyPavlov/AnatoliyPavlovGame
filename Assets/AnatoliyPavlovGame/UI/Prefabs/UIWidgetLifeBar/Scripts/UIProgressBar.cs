﻿using UnityEngine;
using UnityEngine.UI;

public class UIProgressBar : MonoBehaviour
{
    [SerializeField]
    private Image imgFiller;
    [SerializeField]
    private Text textValue;

    public void SetValue(float valueNormalized)
    {
        this.imgFiller.fillAmount = valueNormalized;
        var valueInPercent = Mathf.RoundToInt(valueNormalized * 100f);
        this.textValue.text = $"{valueInPercent}%";
    }

}
