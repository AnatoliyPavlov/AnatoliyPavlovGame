﻿using UnityEngine;

public class UIWidgetLifeBar : MonoBehaviour
{
    [SerializeField]
    private UIProgressBar progressBar;
    private Player player;

    private void OnEnable()
    {
        this.player = GameObject.FindObjectOfType<Player>();
        this.progressBar.SetValue(this.player.healthNormalized);
        this.player.OnPlayerHealthValueChangedEvent += OnPlayerHealthValueChanged;
    }

    private void OnPlayerHealthValueChanged(float newValueNormalized)
    {
        this.progressBar.SetValue(newValueNormalized);
    }

    private void OnDisable()
    {
        if (!this.player.isAlife)
            this.player.OnPlayerHealthValueChangedEvent -= OnPlayerHealthValueChanged;
    }
}
