﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Repository
{
    public virtual void OnCreate() { }
    public abstract void Initialize();
    public virtual void OnStart() { }

    public abstract void Save();
}
