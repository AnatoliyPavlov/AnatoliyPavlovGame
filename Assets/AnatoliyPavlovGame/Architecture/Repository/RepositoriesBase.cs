﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepositoriesBase
{
    private Dictionary<Type, Repository> repositoriesMap;
    private SceneConfig sceneConfig;

    public RepositoriesBase(SceneConfig sceneConfig)
    {
        this.sceneConfig = sceneConfig;
    }

    public void CreateAllRepositorys()
    {
        this.repositoriesMap = this.sceneConfig.CreateAllRepositories();
    }
    public void SendOnCreateToAllRepositories()
    {
        var allRepositorys = this.repositoriesMap.Values;
        foreach (var repository in allRepositorys)
        {
            repository.OnCreate();
        }
    }
    public void InitializeAllRepositories()
    {
        var allRepositorys = this.repositoriesMap.Values;
        foreach (var repository in allRepositorys)
        {
            repository.Initialize();
        }
    }
    public void SendOnStartToAllRepositories()
    {
        var allRepositorys = this.repositoriesMap.Values;
        foreach (var repository in allRepositorys)
        {
            repository.OnStart();
        }
    }
    public T GetRepository<T>() where T : Repository
    {
        var type = typeof(T);
        return (T)this.repositoriesMap[type];
    }
}
