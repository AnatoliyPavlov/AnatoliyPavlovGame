﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneConfigMain : SceneConfig
{
    public override Dictionary<Type, Interactor> CreateAllInteractors()
    {
        var interactorsMap = new Dictionary<Type, Interactor>();
        
        //создаём список интеракторов для этой сцены
        this.CreateInteractor<ScoreInteractor>(interactorsMap);

        return interactorsMap;
    }

    public override Dictionary<Type, Repository> CreateAllRepositories()
    {
        var repositoriesMap = new Dictionary<Type, Repository>();

        //создаем список репозиториев для этой сцены
        this.CreateRepository<ScoreRepository>(repositoriesMap);

        return repositoriesMap;
    }
}
