﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneConfigLoader : MonoBehaviour
{
    public static Scene scene;

    void Start()
    {
        var sceneConfig = new SceneConfigMain();
        scene = new Scene(sceneConfig);
        this.StartCoroutine(scene.InitializeRoutine());
    }
}
