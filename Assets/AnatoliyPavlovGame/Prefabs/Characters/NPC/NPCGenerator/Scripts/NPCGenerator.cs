﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCGenerator : MonoBehaviour
{
    [SerializeField]
    private NPSPool<NPC> npcPool;
    [SerializeField]
    private NPC npcScriptPrefab;
    [SerializeField]
    private int npcPoolCount = 10;
    [SerializeField]
    private Transform thisTransform;
    [SerializeField]
    private GameObject npcPrefab;
    [SerializeField]
    private bool npcPoolAutoExpand = false;
    [SerializeField]
    private Transform cerateNPCPositionFrom;
    [SerializeField]
    private Transform cerateNPCPositionTo;
    [SerializeField]
    private float xToCreateNPCPosition;
    [SerializeField]
    private float zToCreateNPCPosition;
    [SerializeField]
    private Vector3 createNPCPosition;
    [SerializeField]
    private float timeToCreateNPC;
    [SerializeField]
    private float timerToCreateNPC;


    void Start()
    {
        timeToCreateNPC = 2.5f;
        timerToCreateNPC = timeToCreateNPC;
        cerateNPCPositionFrom = GameObject.Find("/PlayingField/Ground/MoveFieldFrom").GetComponent<Transform>();
        cerateNPCPositionTo = GameObject.Find("/PlayingField/Ground/MoveFieldTo").GetComponent<Transform>();
        thisTransform = gameObject.transform;
        npcPrefab = Resources.Load<GameObject>("NPC");
        npcScriptPrefab = npcPrefab.GetComponent<NPC>();
        if (npcPool == null)
        {
            npcPool = new NPSPool<NPC>(npcScriptPrefab, npcPoolCount, thisTransform.parent);
            npcPool.autoExpand = npcPoolAutoExpand;
        }
    }

    void Update()
    {
        if (timerToCreateNPC > 0)
        {
            timerToCreateNPC -= Time.deltaTime;
        }
        else
        {
            if (npcPool.HasFreeElement(out var element))
            {
                this.timerToCreateNPC = this.timeToCreateNPC;
                this.xToCreateNPCPosition = Random.Range(this.cerateNPCPositionFrom.position.x, this.cerateNPCPositionTo.position.x);
                this.zToCreateNPCPosition = Random.Range(this.cerateNPCPositionFrom.position.z, this.cerateNPCPositionTo.position.z);
                this.createNPCPosition = new Vector3(this.xToCreateNPCPosition, 0, this.zToCreateNPCPosition);
                element.transform.position = this.createNPCPosition;
                if(element.isInitialize)
                    element.SetBehaviorByDefault();
            }
        }
    }
}
