﻿using UnityEngine;

[CreateAssetMenu(fileName = "New NPCInfo", menuName = "Gameplay/NPC Info", order = 51)]
public class NPCInfo : ScriptableObject
{
    //[SerializeField]
    //private string _id;
    [SerializeField]
    private float _moveSpeed = 3;
    [SerializeField]
    private float _distanceToMove = 2;
    [SerializeField]
    private float _idleAroundSpeed = 180;
    [SerializeField]
    private float _idleTime = 3;
    [SerializeField]
    private float _distanceToAttack = 10;
    [SerializeField]
    private float _reloadTime = 1;
    [SerializeField]
    private float _bulletSpeed = 15;
    [SerializeField]
    private int _bulletDamage = 50;
    [SerializeField]
    private float _distanceToAggresiveBehavior = 25;


    public float MoveSpeed => this._moveSpeed;

    public float DistanceToMove => this._distanceToMove;

    public float IdleAroundSpeed => this._idleAroundSpeed;

    public float IdleTime => this._idleTime;

    public float DistanceToAttack => this._distanceToAttack;

    public float ReloadTime => this._reloadTime;

    public float BulletSpeed => this._bulletSpeed;

    public int BulletDamage => this._bulletDamage;

    public float DistanceToAggresiveBehavior => this._distanceToAggresiveBehavior;
}