﻿using UnityEngine;

public class MaterialChanger
{
    MeshRenderer meshRender;

    public MaterialChanger(MeshRenderer meshRender, Material material)
    {
        this.meshRender = meshRender;
        this.meshRender.material = material;
    }
}
