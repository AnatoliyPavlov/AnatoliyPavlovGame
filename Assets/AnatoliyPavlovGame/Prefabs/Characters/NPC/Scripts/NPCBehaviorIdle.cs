﻿using UnityEngine;

public class NPCBehaviorIdle : INPCBehavior
{
    private GameObject thisGameObject;
    private NPCInfo npcInfo;
    private Transform thisTransform;
    private Material materialIdle;
    private MeshRenderer meshRender;
    private MaterialChanger materialChanger;
    private float rotateSpeed;
    private float timeIdle;

    public NPCBehaviorIdle(GameObject thisGameObject, NPCInfo npcInfo)
    {
        this.thisGameObject = thisGameObject;
        this.npcInfo = npcInfo;
    }

    public void Enter()
    {
        //Debug.Log("Enter IDLE behavior");
        this.thisTransform = this.thisGameObject.GetComponent<Transform>();
        this.rotateSpeed = this.npcInfo.IdleAroundSpeed;
        this.ChangeMaterialToIdleBehavior();
        this.timeIdle = 1f;
    }

    public void Exit()
    {
        //Debug.Log("Exit IDLE behavior");
    }

    public void Update()
    {
        //Debug.Log("Update IDLE behavior");
    }

    public void FixedUpdate()
    {
        //Debug.Log("FixedUpdate IDLE behavior");
        this.timeIdle -= Time.deltaTime;
        if (this.timeIdle <= 0)
        {
            this.thisGameObject.GetComponent<NPC>().SetBehaviorActive();
        }
        this.TurnAround(this.rotateSpeed);
    }

    private void ChangeMaterialToIdleBehavior()
    {
        this.meshRender = this.thisGameObject.GetComponent<MeshRenderer>();
        this.materialIdle = Resources.Load<Material>("NPCIdleMaterial");
        this.materialChanger = new MaterialChanger(this.meshRender, this.materialIdle);
    }

    private void TurnAround(float speedAround)
    {
        this.thisTransform.Rotate(Vector3.up, speedAround * Time.fixedDeltaTime);
    }
}
