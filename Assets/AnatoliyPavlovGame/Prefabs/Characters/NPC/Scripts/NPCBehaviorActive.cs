﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBehaviorActive : INPCBehavior
{
    private GameObject thisGameObject;
    private NPCInfo npcInfo;
    private Transform thisTransform;
    private Material materialActive;
    private MeshRenderer meshRender;
    private MaterialChanger materialChanger;
    private LineRenderer lineToTarget;
    private Transform moveFieldFrom;
    private Transform moveFieldTo;
    private float xToTarget;
    private float zToTarget;
    private Vector3 targetToMovePosition;
    private float stepToMove;
    private float speedToMove;

    public NPCBehaviorActive(GameObject thisGameObject, NPCInfo npcInfo)
    {
        this.thisGameObject = thisGameObject;
        this.npcInfo = npcInfo;
    }

    public void Enter()
    {
        //Debug.Log("Enter ACTIVE behavior");
        this.thisTransform = this.thisGameObject.GetComponent<Transform>();
        this.lineToTarget = this.thisGameObject.GetComponent<LineRenderer>();
        this.moveFieldFrom = GameObject.Find("/PlayingField/Ground/MoveFieldFrom").GetComponent<Transform>();
        this.moveFieldTo = GameObject.Find("/PlayingField/Ground/MoveFieldTo").GetComponent<Transform>();

        this.speedToMove = this.npcInfo.MoveSpeed;
        ChangeTargetToMove();
        this.lineToTarget.enabled = true;
        ChangeMaterialToActiveBehavior();
    }

    public void Exit()
    {
        //Debug.Log("Exit ACTIVE behavior");
        lineToTarget.enabled = false;
    }

    public void Update()
    {
        //Debug.Log("Update ACTIVE behavior");
    }

    public void FixedUpdate()
    {
        //Debug.Log("FixedUpdate ACTIVE behavior");
        MoveToTarget(targetToMovePosition);
    }

    private void ChangeMaterialToActiveBehavior()
    {
        this.meshRender = this.thisGameObject.GetComponent<MeshRenderer>();
        this.materialActive = Resources.Load<Material>("NPCActiveMaterial");
        this.materialChanger = new MaterialChanger(this.meshRender, this.materialActive);
    }

    public void MoveToTarget(Vector3 targetPosition)
    {
        this.stepToMove = this.speedToMove * Time.fixedDeltaTime;
        if (Vector3.Distance(this.thisTransform.position, targetPosition) > 0.1f)
        {
            this.thisTransform.position = Vector3.MoveTowards(this.thisTransform.position, targetPosition, this.stepToMove);
            LineStartToTargetRender(thisTransform.position);
        }
        else
        {
            this.thisGameObject.GetComponent<NPC>().SetBehaviorIdle();
        }
    }

    private void ChangeTargetToMove()
    {
        xToTarget = Random.Range(moveFieldFrom.position.x, moveFieldTo.position.x);
        zToTarget = Random.Range(moveFieldFrom.position.z, moveFieldTo.position.z);
        targetToMovePosition = new Vector3(xToTarget, 0, zToTarget);
        LineEndToTargetRender(targetToMovePosition);
        LookToTarget(targetToMovePosition);
    }

    //======Построение линии для красоты
    private void LineStartToTargetRender(Vector3 startPosition)
    {
        lineToTarget.SetPosition(0, startPosition);
    }

    private void LineEndToTargetRender(Vector3 endPosition)
    {
        lineToTarget.SetPosition(1, endPosition);
    }
    //======Построение линии для красоты

    private void LookToTarget(Vector3 lookPosition)
    {
        Vector3 relativePos = this.thisTransform.position - lookPosition;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        thisTransform.rotation = rotation;
    }
}
