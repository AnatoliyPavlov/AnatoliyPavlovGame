﻿public interface INPCBehavior
{
    void Enter();

    void Exit();

    void Update();

    void FixedUpdate();
}
