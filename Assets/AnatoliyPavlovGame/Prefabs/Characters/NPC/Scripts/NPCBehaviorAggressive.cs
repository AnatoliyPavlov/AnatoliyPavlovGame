﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBehaviorAggressive : INPCBehavior
{
    private GameObject thisGameObject;
    private NPCInfo npcInfo;
    private Transform thisTransform;
    private Material materialAggressive;
    private MeshRenderer meshRender;
    private MaterialChanger materialChanger;
    private LineRenderer lineToTarget;
    private Transform targetToMoveTransform;
    private Vector3 targetToMovePosition;
    private float stepToMove;
    private float speedToMove;
    private float distanceToMove;
    private GameObject bulletPrefab;
    private float reloadTime;
    private float reloadingTimer;
    private float distanceToShot;
    private Vector3 shotRotation;

    private int npcBulletPoolCount = 3;
    private bool npcBulletPoolAutoExpand = true;
    private BulletMove bulletMovePrefab;
    private NPCBulletPool<BulletMove> npcBulletPool;

    public NPCBehaviorAggressive(GameObject thisGameObject, NPCInfo npcInfo)
    {
        this.thisGameObject = thisGameObject;
        this.npcInfo = npcInfo;
    }

    public void Enter()
    {
        //Debug.Log("Enter AGGRESSIVE behavior");
        this.thisTransform = this.thisGameObject.GetComponent<Transform>();
        this.lineToTarget = this.thisGameObject.GetComponent<LineRenderer>();
        this.speedToMove = this.npcInfo.MoveSpeed;
        this.distanceToMove = this.npcInfo.DistanceToMove;
        this.reloadTime = this.npcInfo.ReloadTime;
        this.distanceToShot = this.npcInfo.DistanceToAttack;
        this.targetToMoveTransform = GameObject.FindObjectOfType<Player>().GetComponent<Transform>();
        this.bulletPrefab = Resources.Load<GameObject>("Bullet");
        this.LineToTargetRenderEnabled();
        this.ChangeMaterialToAggressiveBehavior();
        this.bulletMovePrefab = this.bulletPrefab.GetComponent<BulletMove>();
        if (this.npcBulletPool == null)
        {
            this.npcBulletPool = new NPCBulletPool<BulletMove>(this.bulletMovePrefab, this.npcBulletPoolCount, this.thisGameObject.transform.parent);
            this.npcBulletPool.autoExpand = this.npcBulletPoolAutoExpand;
        }
    }

    public void Exit()
    {
        //Debug.Log("Exit AGGRESSIVE behavior");
        this.LineToTargetRenderDisabled();
    }

    public void Update()
    {
        //Debug.Log("Update AGGRESSIVE behavior");
    }

    public void FixedUpdate()
    {
        //Debug.Log("FixedUpdate AGGRESSIVE behavior");
        this.targetToMovePosition = this.targetToMoveTransform.position;
        this.MoveToTarget(this.targetToMovePosition);
        this.ShotIfCan();
    }

    private void ChangeMaterialToAggressiveBehavior()
    {
        this.meshRender = this.thisGameObject.GetComponent<MeshRenderer>();
        this.materialAggressive = Resources.Load<Material>("NPCAggressiveMaterial");
        this.materialChanger = new MaterialChanger(this.meshRender, this.materialAggressive);
    }

    public void MoveToTarget(Vector3 targetPosition)
    {
        this.stepToMove = this.speedToMove * Time.fixedDeltaTime;
        if (Vector3.Distance(this.thisTransform.position, targetPosition) > this.distanceToMove)
        {
            this.thisTransform.position = Vector3.MoveTowards(this.thisTransform.position, targetPosition, this.stepToMove);
        }
        this.LineStartToTargetRender(thisTransform.position);
        this.LineEndToTargetRender(targetPosition);
        this.LookToTarget(targetPosition);
    }

    private void LookToTarget(Vector3 lookPosition)
    {
        Vector3 shotRotation = this.thisTransform.position - lookPosition;
        Quaternion rotation = Quaternion.LookRotation(shotRotation, Vector3.up);
        this.thisTransform.rotation = rotation;
    }

    private void ShotIfCan()
    {
        if (this.reloadingTimer > 0)
        {
            this.reloadingTimer -= Time.fixedDeltaTime;
        }
        else
        {
            if (Vector3.Distance(this.thisTransform.position, this.targetToMovePosition) <= this.distanceToShot)
            {
                this.shotRotation = this.targetToMovePosition - this.thisTransform.position;
                this.ShotToTarget(Quaternion.LookRotation(this.shotRotation, Vector3.up));
                this.reloadingTimer = this.reloadTime;
            }
        }
    }

    public void ShotToTarget(Quaternion shotDirection)
    {
        var bullet = this.npcBulletPool.GetFreeElement();
        bullet.transform.position = this.thisTransform.position;
        bullet.transform.rotation = shotDirection;
    }

    //======Построение линии для красоты
    private void LineToTargetRenderEnabled()
    {
        this.lineToTarget.enabled = true;
    }
    private void LineToTargetRenderDisabled()
    {
        this.lineToTarget.enabled = false;
    }
    private void LineStartToTargetRender(Vector3 startPosition)
    {
        this.lineToTarget.SetPosition(0, startPosition);
    }

    private void LineEndToTargetRender(Vector3 endPosition)
    {
        this.lineToTarget.SetPosition(1, endPosition);
    }
    //======Построение линии для красоты
}
