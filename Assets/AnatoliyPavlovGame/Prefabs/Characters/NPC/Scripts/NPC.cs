﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    private Dictionary<Type, INPCBehavior> behaviorsMap;
    private INPCBehavior behaviorCurrent;
    private LineRenderer lineToTarget;
    private Transform thisTransform;
    private MeshRenderer meshRenderer;
    private NPCInfo npcInfo;
    private GameObject thisGameObject;
    private Player playerScript;
    private AudioSource dieSound;
    public bool isInitialize { get;private set;}

    private void Start()
    {
        this.lineToTarget = this.gameObject.GetComponent<LineRenderer>();
        this.thisTransform = this.gameObject.GetComponent<Transform>();
        this.meshRenderer = this.gameObject.GetComponent<MeshRenderer>();
        this.npcInfo = Resources.Load<NPCInfo>("NPCInfo");
        this.thisGameObject = this.gameObject;
        this.dieSound = GameObject.Find("/Camera/SoundsMisc").GetComponent<AudioSource>();
        this.InitBehaviors();
        this.SetBehaviorByDefault();
        //Подписка на событие смерти игрока
        this.playerScript = GameObject.FindObjectOfType<Player>();
        this.playerScript.OnPlayerKillEvent += this.SetBehaviorIdle;
        this.playerScript.OnPlayerKillEvent += this.Otpiska;
        //
        this.isInitialize = true;
    }

    private void FixedUpdate()
    {
        if (this.behaviorCurrent != null)
            this.behaviorCurrent.FixedUpdate();
    }

    private void InitBehaviors()
    {
        this.behaviorsMap = new Dictionary<Type, INPCBehavior>();
        //Здесь просто добавляем поведения
        this.behaviorsMap[typeof(NPCBehaviorIdle)] = new NPCBehaviorIdle(this.thisGameObject, this.npcInfo);
        this.behaviorsMap[typeof(NPCBehaviorActive)] = new NPCBehaviorActive(this.thisGameObject, this.npcInfo);
        this.behaviorsMap[typeof(NPCBehaviorAggressive)] = new NPCBehaviorAggressive(this.thisGameObject, this.npcInfo);
        //Здесь просто добавляем поведения
    }

    private void SetBehavior(INPCBehavior newBehavior)
    {
        if (this.behaviorCurrent != null)
        {
            if (this.behaviorCurrent != newBehavior)
            {
                this.behaviorCurrent.Exit();
                this.behaviorCurrent = newBehavior;
                this.behaviorCurrent.Enter();
            }
        }
        else
        {
            this.behaviorCurrent = newBehavior;
            this.behaviorCurrent.Enter();
        }
    }

    public void SetBehaviorByDefault()
    {
        this.SetBehaviorIdle();
    }

    private INPCBehavior GetBehavior<T>() where T : INPCBehavior
    {
        var type = typeof(T);
        return this.behaviorsMap[type];
    }

    public void SetBehaviorIdle()
    {
        var behavior = this.GetBehavior<NPCBehaviorIdle>();
        this.SetBehavior(behavior);
    }

    public void SetBehaviorActive()
    {
        var behavior = this.GetBehavior<NPCBehaviorActive>();
        this.SetBehavior(behavior);
    }

    public void SetBehaviorAggressive()
    {
        var behavior = this.GetBehavior<NPCBehaviorAggressive>();
        this.SetBehavior(behavior);
    }

    private void Otpiska()
    {
        this.playerScript.OnPlayerKillEvent -= SetBehaviorActive;
        this.playerScript.OnPlayerKillEvent -= Otpiska;
    }
    public void Die()
    {
        if (Score.isInitialized && CurrentScore.isInitialized)
        {
            CurrentScore.AddScore(1);
            Score.AddScore(this, 1);
            Debug.Log($"Score: {Score.score}");
        }
        this.dieSound.Play();
        this.gameObject.SetActive(false);
    }
}
