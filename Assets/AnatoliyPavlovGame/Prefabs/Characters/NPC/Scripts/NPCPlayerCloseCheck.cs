﻿using UnityEngine;

public class NPCPlayerCloseCheck : MonoBehaviour
{
    private void Start()
    {
        Initialize();
    }
    private void OnTriggerEnter(Collider other)
    {
            if (other.gameObject.name == "Player" || other.gameObject.name == "Player(Clone)")
                if (other.gameObject.GetComponent<Player>().isAlife)
                    this.gameObject.transform.parent.GetComponent<NPC>().SetBehaviorAggressive();
    }
    private void OnTriggerExit(Collider other)
    {
            if (other.gameObject.name == "Player" || other.gameObject.name == "Player(Clone)")
                this.gameObject.transform.parent.GetComponent<NPC>().SetBehaviorActive();
    }
    private void Initialize()
    {
        var npcInfo = Resources.Load<NPCInfo>("NPCInfo");
        gameObject.GetComponent<SphereCollider>().radius = npcInfo.DistanceToAggresiveBehavior;
    }
}
