﻿using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private int health;
    [SerializeField]
    private int healthDefault = 100;
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private Transform thisTransform;
    [SerializeField]
    private float speed;
    [SerializeField]
    public bool isAlife = true;
    [SerializeField]
    private float deathTime;
    [SerializeField]
    private float deathTimer;
    public event Action OnPlayerKillEvent;
    public event Action<float> OnPlayerHealthValueChangedEvent;
    public float healthNormalized => (float)this.health / this.healthDefault;
    private Joystick joystick;

    private void Awake()
    {
        this.health = this.healthDefault;
        this.OnPlayerHealthValueChangedEvent?.Invoke(this.healthNormalized);
    }

    private void Start()
    {
        this.health = 100;
        this.thisTransform = this.gameObject.GetComponent<Transform>();
        this.speed = 7.5f;
        this.joystick = FindObjectOfType<Joystick>();
        if (deathTime <= 0)
            deathTime = 5f;
        this.deathTimer = this.deathTime;
    }

    private void Update()
    {
        if (!isAlife)
        {
            deathTimer -= Time.deltaTime;
            if (deathTimer <= 0)
            {
                this.RestartScene();
            }
        }
    }

    private void FixedUpdate()
    {
        if (this.joystick.isActiveAndEnabled)
            this.Move();
    }

    public void PlayerHit(int valueHit)
    {
        this.health -= valueHit;
        if (this.health <= 0)
            this.Death();
        this.OnPlayerHealthValueChangedEvent?.Invoke(this.healthNormalized);
    }

    private void Move()
    {
        this.thisTransform.position += new Vector3(this.joystick.Direction.x, 0, this.joystick.Direction.y) * Time.fixedDeltaTime * this.speed;
    }

    public void Death()
    {
        this.OnPlayerKillEvent?.Invoke();
        this.isAlife = false;
        this.gameObject.GetComponent<MeshRenderer>().enabled = false;
        this.gameObject.GetComponent<Collider>().enabled = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "NPC"|| collision.gameObject.name == "NPC(Clone)")
        {
            collision.gameObject.GetComponent<NPC>().Die();
        }
    }
    private void RestartScene()
    {
        SceneLoader sceneLoader = new SceneLoader();
        sceneLoader.RestarScene();
    }
}
