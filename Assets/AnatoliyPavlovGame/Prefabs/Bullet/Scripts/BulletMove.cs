﻿using UnityEngine;

public class BulletMove : MonoBehaviour
{
    private Transform thisTransform;
    private float stepMoving;
    private float speedMoving;
    private NPCInfo npcInfo;

    private void Start()
    {
        this.thisTransform = this.gameObject.GetComponent<Transform>();
        this.npcInfo = Resources.Load<NPCInfo>("NPCInfo");
        this.speedMoving = this.npcInfo.BulletSpeed;
    }

    private void FixedUpdate()
    {
        this.stepMoving = this.speedMoving * Time.fixedDeltaTime;
        this.thisTransform.position += this.thisTransform.forward * this.speedMoving * Time.fixedDeltaTime;
    }
}
