﻿using UnityEngine;

public class BulletTrigger : MonoBehaviour
{
    private NPCInfo npcInfo;
    public int hitDamage;

    private void Start()
    {
        this.npcInfo = Resources.Load<NPCInfo>("NPCInfo");
        this.hitDamage = npcInfo.BulletDamage;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player" || other.gameObject.name == "Player(Clone)")
        {
            other.GetComponent<Player>().PlayerHit(this.hitDamage);
            this.Disable();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "EndCube")
        {
            this.Disable();
        }
    }

    private void Disable()
    {
        this.gameObject.transform.parent.gameObject.SetActive(false);
    }
}
