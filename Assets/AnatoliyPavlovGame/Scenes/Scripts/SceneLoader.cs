﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader
{
    private void LoadScene(int buildIndex)
    {
        if (buildIndex < SceneManager.sceneCountInBuildSettings)
        SceneManager.LoadScene(buildIndex);
    }

    public void LoadNextScene()
    {
        var buildIndexNext = SceneManager.GetActiveScene().buildIndex + 1;
        if (buildIndexNext < SceneManager.sceneCountInBuildSettings)
            SceneManager.LoadScene(buildIndexNext);
    }

    public void RestarScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void LoadStartScene()
    {
        SceneManager.LoadScene(0);
    }
}
